getIds3
===
	select #page("*")#  from user  where #use("cols")#

cols	
===

	name = #name#
	
findOne
===

	select * from user where id = ${id}

	
select  
===

	select 
	*
	from user where 1=1 
	#use("condition")#
	
condition	
===	

	@if(isNotEmpty(name)){
	and name = '${name}'
	@}
	
	
	
selectUserAndDepartment
===
    select * from user where 1=1
    @ orm.lazyMany({"id":"userId"},"wan.user.selectRole","Role",{'alias':'myRoles'});

selectRole
===

    select r.* from user_role ur left join role r on ur.role_id=r.id
    where ur.user_id=#userId# 
    @ /* and state=#state# */

batchUpdate    
===

	update user set department_id = 1 where id  in ( #join(users,"id")#)


getCount
===

    select count(1) from user where name = #name#